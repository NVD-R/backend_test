<?php

return [
    'notifications' => [
        'mail' => [
            'verify' => [
                'subject' => 'Verify Email Address',
                'greeting' => 'Hello!',
                'body' => 'Please click the button below to verify your email address.',
                'button' => 'Verify Email Address',
                'footer' => 'If you did not create an account, no further action is required.',
                'baseUrl' => 'http://appardakht.mobtakerteam.com/dashboard/register/confirmation'
                // 'link' => "اگر نمیتوانید بر روی دکمه \":actionText\" کلیک کنید, لینک زیر را در مرورگر خود کپی کنید\n".
                //     ': [:displayableActionUrl](:actionURL)',
                // 'link' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
                //     'into your web browser: [:displayableActionUrl](:actionURL)',
            ],
            'secure_code' => [
                'subject' => 'Secure Code',
                'greeting' => 'Hello!',
                'body' => 'Please enter the code in app.',
                'button' => 'null',
                'footer' => 'If you did not request, no further action is required.',
                'baseUrl' => 'http://appardakht.mobtakerteam.com/dashboard/register/confirmation',
                'type' => 'secure_code'
                // 'link' => "اگر نمیتوانید بر روی دکمه \":actionText\" کلیک کنید, لینک زیر را در مرورگر خود کپی کنید\n".
                //     ': [:displayableActionUrl](:actionURL)',
                // 'link' => "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
                //     'into your web browser: [:displayableActionUrl](:actionURL)',
            ],
            'otp' => [
                'subject' => 'Secret Code',
                'greeting' => 'Hello!',
                'body' => 'Please click the button below to login.',
                'button' => 'login',
                'footer' => 'If you did not create an account, no further action is required.',
                'baseUrl' => 'http://appardakht.mobtakerteam.com/dashboard/login/reset_password'
            ],
        ]
    ]
];
