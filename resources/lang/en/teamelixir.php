<?php

return [
    '1' => [
        '1' => [
            'elixir' => 16,
            'hero_elixir' => 64,
            'hero_gem' => 1,
        ],
        '2' => [
            'elixir' => 20,
            'hero_elixir' => 64,
            'hero_gem' => 1,
        ],
        '3' => [
            'elixir' => 24,
            'hero_elixir' => 64,
            'hero_gem' => 1,
        ],
        '4' => [
            'elixir' => 32,
            'hero_elixir' => 64,
            'hero_gem' => 1,
        ],
    ],
    '2' => [
        '1' => [
            'elixir' => 24,
            'hero_elixir' => 72,
            'hero_gem' => 2,
        ],
        '2' => [
            'elixir' => 28,
            'hero_elixir' => 72,
            'hero_gem' => 2,
        ],
        '3' => [
            'elixir' => 32,
            'hero_elixir' => 72,
            'hero_gem' => 2,
        ],
        '4' => [
            'elixir' => 40,
            'hero_elixir' => 72,
            'hero_gem' => 2,
        ],
    ],
    '3' => [
        '1' => [
            'elixir' => 32,
            'hero_elixir' => 80,
            'hero_gem' => 3,
        ],
        '2' => [
            'elixir' => 36,
            'hero_elixir' => 80,
            'hero_gem' => 3,
        ],
        '3' => [
            'elixir' => 40,
            'hero_elixir' => 80,
            'hero_gem' => 3,
        ],
        '4' => [
            'elixir' => 48,
            'hero_elixir' => 80,
            'hero_gem' => 3,
        ],
    ],
    '4' => [
        '1' => [
            'elixir' => 40,
            'hero_elixir' => 88,
            'hero_gem' => 4,
        ],
        '2' => [
            'elixir' => 44,
            'hero_elixir' => 88,
            'hero_gem' => 4,
        ],
        '3' => [
            'elixir' => 48,
            'hero_elixir' => 88,
            'hero_gem' => 4,
        ],
        '4' => [
            'elixir' => 56,
            'hero_elixir' => 88,
            'hero_gem' => 4,
        ],
    ],
    '5' => [
        '1' => [
            'elixir' => 48,
            'hero_elixir' => 96,
            'hero_gem' => 5,
        ],
        '2' => [
            'elixir' => 52,
            'hero_elixir' => 96,
            'hero_gem' => 5,
        ],
        '3' => [
            'elixir' => 56,
            'hero_elixir' => 96,
            'hero_gem' => 5,
        ],
        '4' => [
            'elixir' => 64,
            'hero_elixir' => 96,
            'hero_gem' => 5,
        ],
    ],
    '6' => [
        '1' => [
            'elixir' => 56,
            'hero_elixir' => 102,
            'hero_gem' => 6,
        ],
        '2' => [
            'elixir' => 60,
            'hero_elixir' => 102,
            'hero_gem' => 6,
        ],
        '3' => [
            'elixir' => 64,
            'hero_elixir' => 102,
            'hero_gem' => 6,
        ],
        '4' => [
            'elixir' => 72,
            'hero_elixir' => 102,
            'hero_gem' => 6,
        ],
    ],
    '7' => [
        '1' => [
            'elixir' => 64,
            'hero_elixir' => 110,
            'hero_gem' => 7,
        ],
        '2' => [
            'elixir' => 68,
            'hero_elixir' => 110,
            'hero_gem' => 7,
        ],
        '3' => [
            'elixir' => 72,
            'hero_elixir' => 110,
            'hero_gem' => 7,
        ],
        '4' => [
            'elixir' => 80,
            'hero_elixir' => 110,
            'hero_gem' => 7,
        ],
    ],
    '8' => [
        '1' => [
            'elixir' => 72,
            'hero_elixir' => 118,
            'hero_gem' => 8,
        ],
        '2' => [
            'elixir' => 76,
            'hero_elixir' => 118,
            'hero_gem' => 8,
        ],
        '3' => [
            'elixir' => 80,
            'hero_elixir' => 118,
            'hero_gem' => 8,
        ],
        '4' => [
            'elixir' => 88,
            'hero_elixir' => 118,
            'hero_gem' => 8,
        ],
    ],
    '9' => [
        '1' => [
            'elixir' => 80,
            'hero_elixir' => 126,
            'hero_gem' => 9,
        ],
        '2' => [
            'elixir' => 84,
            'hero_elixir' => 126,
            'hero_gem' => 9,
        ],
        '3' => [
            'elixir' => 88,
            'hero_elixir' => 126,
            'hero_gem' => 9,
        ],
        '4' => [
            'elixir' => 96,
            'hero_elixir' => 126,
            'hero_gem' => 9,
        ],
    ],
    '10' => [
        '1' => [
            'elixir' => 88,
            'hero_elixir' => 132,
            'hero_gem' => 10,
        ],
        '2' => [
            'elixir' => 92,
            'hero_elixir' => 132,
            'hero_gem' => 10,
        ],
        '3' => [
            'elixir' => 96,
            'hero_elixir' => 132,
            'hero_gem' => 10,
        ],
        '4' => [
            'elixir' => 104,
            'hero_elixir' => 132,
            'hero_gem' => 10,
        ],
    ],
    '11' => [
        '1' => [
            'elixir' => 96,
            'hero_elixir' => 140,
            'hero_gem' => 11,
        ],
        '2' => [
            'elixir' => 100,
            'hero_elixir' => 140,
            'hero_gem' => 11,
        ],
        '3' => [
            'elixir' => 104,
            'hero_elixir' => 140,
            'hero_gem' => 11,
        ],
        '4' => [
            'elixir' => 112,
            'hero_elixir' => 140,
            'hero_gem' => 11,
        ],
    ],
    '12' => [
        '1' => [
            'elixir' => 102,
            'hero_elixir' => 148,
            'hero_gem' => 12,
        ],
        '2' => [
            'elixir' => 108,
            'hero_elixir' => 148,
            'hero_gem' => 12,
        ],
        '3' => [
            'elixir' => 112,
            'hero_elixir' => 148,
            'hero_gem' => 12,
        ],
        '4' => [
            'elixir' => 120,
            'hero_elixir' => 148,
            'hero_gem' => 12,
        ],
    ],
];
