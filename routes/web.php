<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Libs\Streamer\VideoStreamer;

// use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('/samples/import', ['uses' => 'GenerateJsonController@importExcel']);
$router->post('/generate', ['uses' => 'GenerateJsonController@jsonGenerate']);

$router->group(['middleware' => 'client'], function () use ($router) {
    $router->group(['middleware' => 'auth'], function () use ($router) {
    });
});