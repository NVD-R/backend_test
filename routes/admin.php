<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Libs\Streamer\VideoStreamer;

// use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['middleware' => 'client'], function () use ($router) {
    $router->group(['prefix' => 'admin', 'middleware' => 'auth:admin'], function () use ($router) {
    });
});