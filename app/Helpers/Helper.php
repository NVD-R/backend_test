<?php
if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param  string $path
     * @return string
     */
    function app_path($path = '')
    {
        return app('path') . ($path ? DIRECTORY_SEPARATOR . $path : $path);
    }
}

if (!function_exists('urlGenerator')) {
    /**
     * @return \Laravel\Lumen\Routing\UrlGenerator
     */
    function urlGenerator()
    {
        return new \Laravel\Lumen\Routing\UrlGenerator(app());
    }
}

if (!function_exists('asset')) {
    /**
     * @param $path
     * @param bool $secured
     *
     * @return string
     */
    function asset($path, $secured = false)
    {
        return urlGenerator()->asset($path, $secured);
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }
}

if (!function_exists('jsonEn')) {
    function jsonEn($data)
    {
        return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}
if (!function_exists('jsonDe')) {
    function jsonDe($data, $returnArray = false)
    {
        return json_decode($data, $returnArray);
    }
}

if (!function_exists('change_mobile_format')) {
    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    function change_mobile_format($mobile)
    {
        ltrim($mobile, '+');
        return '00' . ltrim($mobile, '+');
    }
}
