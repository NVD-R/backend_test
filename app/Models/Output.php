<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Output extends Model
{
    use FullTextSearch, SoftDeletes;

    protected $searchable = [
        'table',
    ];

    protected $fillable = [
        'table',
        'columns',
        'file',
    ];
    protected $casts = [
        'columns' => 'object'
    ];
}