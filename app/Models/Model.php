<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    public function scopeOrderBys($query, $orders)
    {
        foreach (json_decode($orders, true) as $field => $direction) {
            $query = $query->orderBy($field, $direction);
        }
        return $query;
    }
}
