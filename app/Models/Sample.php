<?php

namespace App\Models;

use App\Casts\Category;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sample extends Model
{
    use FullTextSearch, SoftDeletes;

    protected $searchable = [
        'product',
    ];

    protected $fillable = [
        'product',
        'category',
        'amount',
        'date',
        'country',
    ];
    protected $casts = [
        'category' => Category::class
    ];
}