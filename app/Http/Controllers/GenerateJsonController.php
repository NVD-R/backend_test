<?php

namespace App\Http\Controllers;

use App\Exceptions\Exception;
use Illuminate\Http\Request;
use App\Models\Output;
use App\Http\Controllers\Controller;
use App\Imports\SampleImport;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class GenerateJsonController extends Controller
{
    public function importExcel(Request $r)
    {
        $v = $this->validate($r, [
            'sample_file' => 'required|file',
        ]);
        $sampleUrl = $r->file('sample_file')->store('sample');
        (new SampleImport())->import($sampleUrl);
        return $this->response();
    }



    public function jsonGenerate(Request $r)
    {
        $v = $this->validate($r, [
            'model' => 'required|string',
            'is_model' => 'required|boolean',
            'columns' => 'required|array'
        ]);
        if ($v['is_model']) {
            $model = 'App\Models\\' . Str::ucfirst($v['model']);
            $model = new $model();
            $table = $model->getTable();
        } else {
            $model = \DB::table($v['model']);
            $table = $v['model'];
        }
        $orders = $v['columns'];
        foreach ($orders as $order) {
            $model = $model->orderBy($order);
        }
        $res = [];
        $model->chunk(10, function ($samples) use ($orders, &$res) {
            $res = array_merge_recursive($res, $samples->groupBy($orders)->toArray());
        });

        if (!Storage::put($fileUrl = 'output/' . Str::random() . '.json', json_encode($res))) {
            throw new Exception(40020);
        }
        $output = new Output();
        $output->table = $table;
        $output->columns = $v['columns'];
        $output->file = $fileUrl;
        $output->save();
        return $this->response($res);
    }
}