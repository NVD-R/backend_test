<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    protected function response(
        $data = null,
        int $statusCode = 200,
        array $headers = [],
        int $jsonNumericCheck = 0/* JSON_NUMERIC_CHECK*/
    ): object {
        $data = [
            'status' => true,
            'message' => trans('exception.20000')[0],
            'result' => $data,
        ];
        return response()->json($data, $statusCode, $headers, $jsonNumericCheck);
        // return new JsonResponse($data, $statusCode, $headers, $jsonNumericCheck);
    }
}
