<?php

namespace App\Http\Middleware;

use Closure;
use App\Exceptions\Exception;

class ClientMiddleware
{
    public function handle(object $request, Closure $next, bool $guard = null): object
    {
        $client = collect(config('app.apps'))->where('app_id', $request->header('App-Id'))
            ->where('app_key', $request->header('App-Key'))->first();

        if (!$client) {
            throw new Exception(40103);
        }

        $request->request->add(['client_id' => $client['id']]);

        return $next($request);
    }
}
