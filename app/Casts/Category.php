<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class Category implements CastsAttributes
{
    public function get($model, $key, $value, $attributes)
    {
        if ($value == 0) {
            return 'Vegetables';
        } elseif ($value == 1) {
            return 'Fruit';
        }
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  array  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        if ($value == 'Vegetables') {
            return 0;
        } elseif ($value == 'Fruit') {
            return 1;
        }
    }
}