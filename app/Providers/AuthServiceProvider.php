<?php

namespace App\Providers;

use App\Models\Token;
use App\Models\Admin\Token as AdminToken;
use Illuminate\Support\ServiceProvider;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            if (
                $request->bearerToken() and
                $token = Token::select('user_id')->with(['user'])->where([
                    'access_token' => $request->bearerToken(),
                    // ['access_token_expired_at', '>', Carbon::now()]
                ])->first()
            ) {
                return $token->user;
            }
        });
        $this->app['auth']->viaRequest('admin', function ($request) {
            if (
                $request->bearerToken() and
                $token = AdminToken::select('user_id')->with(['user'])->where([
                    'access_token' => $request->bearerToken(),
                    // ['access_token_expired_at', '>', Carbon::now()]
                ])->first()
            ) {
                $user = $token->user;
                return $user;
            }
        });
    }
}
