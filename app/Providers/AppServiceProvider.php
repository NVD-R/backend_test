<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (app()->environment('local')) {
            app()->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            app()->register(\Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
        }
    }
}
