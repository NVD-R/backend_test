<?php

namespace App\Imports;

use App\Models\Sample;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;

class SampleImport implements ToCollection, WithCustomCsvSettings
{
    use Importable;

    public function collection(Collection $rows)
    {
        $isHeader = true;
        foreach ($rows as $row) {
            if ($isHeader) {
                $isHeader = false;
                continue;
            }
            $s = new Sample();
            $s->product = $row[1];
            $s->category = $row[2];
            $s->amount = $row[3];
            $s->date = $row[4];
            $s->country = $row[5];
            $s->save();
        }
    }
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ";"
        ];
    }
}