<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use CustomException;
use Throwable;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WebSocketHandler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];
    protected $dontReportError = [
        40601, 40602, 40603, 40101, 40102, 40103, 40104, 40105, 40301, 40302, 40001, 40002, 40003, 40004, 40401, 50001
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        if (app()->bound('sentry')) {
            if ($exception instanceof Exception and !in_array($exception->getError(), $this->dontReportError)) {
                app('sentry')->captureException($exception);
            } elseif (!$exception instanceof ValidationException) {
                app('sentry')->captureException($exception);
            }
        }
        // if (app()->bound('sentry') && !in_array($exception->getError(), $this->dontReportError)) {
        //     app('sentry')->captureException($exception);
        // }
        // if (app()->bound('sentry') && $this->shouldReport($exception)) {
        //     app('sentry')->captureException($exception);
        // }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception, $fds = [])
    {
        // return parent::render($request, $exception);
        if ($exception instanceof Exception && $exception->getError()) {
            return $this->response($exception->getError(), $exception->getMessage(), $exception->getFds() ?? $fds);
        } elseif ($exception instanceof ValidationException) {
            return $this->response(40000, trans('exception.40000')[0], $fds, $exception->errors());
        }
        //  elseif ($exception->getStatusCode() == 404) {
        //     return $this->response(40400, trans('exception.40401')[0], $exception->getMessage());
        // }
        // $rendered = parent::render($request, $exception);
        return $this->response(50000, $exception->getMessage(), $fds);
    }

    public function renderForConsole($output, Throwable $exception)
    {
        if ($exception instanceof Exception && $exception->getError()) {
            $this->responseForConsole($output, $exception->getError(), $exception->getMessage());
        } elseif ($exception instanceof ValidationException) {
            $this->responseForConsole($output, 40000, $exception->errors());
        } else {
            (new ConsoleApplication)->renderThrowable($exception, $output);
        }
    }

    public function response($error, $message, array $fds, $result = null)
    {
        $swoole = app('swoole');
        foreach ($fds as $fd) {
            if ($swoole->isEstablished($fd)) {
                $swoole->push($fd, jsonEn([
                    'status' => false,
                    'error' => $error,
                    'message' => $message,
                    'result' => $result,
                ]));
            }
        }
        return;
    }
    public function responseForConsole($output, $error, $message, $result = null)
    {
        $output->writeln(sprintf(
            '<info>%s</info>',
            jsonEn([
                'status' => false,
                'error' => $error,
                'message' => $message,
                'result' => $result,
            ])
        ));
    }
}
