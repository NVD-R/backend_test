<?php

namespace App\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{

    private $error;
    private $fds;
    protected $message;

    public function __construct(int $error, array $fds = [], array $message = [])
    {
        $this->error = $error;
        $this->fds = $fds;

        // $this->message = $message;
        $key = 'exception.' . $error;
        if (empty($message)) {
            if ($key != ($value = trans($key))) {
                $this->message = $value[0];
            }
        }

        if (array_key_exists('errors', $message)) {
            if ($key != ($value = trans($key))) {
                $this->message = str_replace(array_keys($message['errors']), $message['errors'], $value[0]);
                // $this->message = $message;
            }
        }
    }

    public function getError(): int
    {
        return $this->error;
    }
    public function getFds(): array
    {
        return $this->fds;
    }
    // public function getMessages(): array
    // {
    //     return $this->message['text'];
    //     // $messages = [];
    //     // foreach ($this->messages as $key => $message) {
    //     //     $messages['text_' . $key] = $message;
    //     // }
    //     // return $messages;
    // }
}
