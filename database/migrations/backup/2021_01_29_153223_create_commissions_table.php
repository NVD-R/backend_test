<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('exchange_id');
            $table->unsignedInteger('currency_id');
            $table->unsignedTinyInteger('transaction_type')->default(2);
            $table->unsignedTinyInteger('user_type')->default(0);
            $table->decimal('total_percent', 40, 20)->default(0);
            $table->decimal('referral_percent', 40, 20)->default(0);
            $table->unsignedInteger('referral_currency_id');
            $table->decimal('cashback_percent', 40, 20)->default(0);
            $table->unsignedInteger('cashback_currency_id');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
