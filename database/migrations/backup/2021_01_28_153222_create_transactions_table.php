<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('from_user_id');
            $table->unsignedBigInteger('from_wallet_id');
            $table->string('from_wallet_address');
            $table->unsignedBigInteger('from_order_id')->nullable();
            $table->unsignedBigInteger('to_user_id')->nullable();
            $table->unsignedBigInteger('to_wallet_id')->nullable();
            $table->string('to_wallet_address');
            $table->unsignedBigInteger('to_order_id')->nullable();
            $table->unsignedInteger('currency_id');
            $table->decimal('amount', 40, 20)->default(0);
            $table->boolean('is_fiat')->default(false);
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->unsignedTinyInteger('network')->default(0);
            $table->string('tx_id')->nullable();
            $table->json('error')->nullable();
            $table->json('notif')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
