<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id');
            $table->unsignedInteger('first_currency_id');
            $table->unsignedBigInteger('first_wallet_id');
            $table->unsignedInteger('second_currency_id');
            $table->unsignedBigInteger('second_wallet_id');
            $table->boolean('is_sell');
            $table->boolean('is_reverse')->default(false);
            $table->decimal('rate', 40, 20);
            $table->decimal('amount', 40, 20);
            $table->decimal('remaining_amount', 40, 20);
            $table->unsignedBigInteger('exchange_id');
            $table->unsignedTinyInteger('type')->default(0);
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();

            // $table->boolean('ask_is_fiat');
            // $table->unsignedInteger('pay_id');
            // $table->boolean('pay_is_fiat');
            // $table->decimal('order_pay_amount', 40, 20);
            // $table->decimal('commission', 40, 20);
            // $table->string('payment_receipt');
            // $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
