<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->id('id');
            $table->string('name')->nullable();
            $table->unsignedBigInteger('owner_id');
            $table->string('private_key', 500);
            $table->string('public_key');
            $table->string('address');
            $table->string('address_hex');
            $table->unsignedTinyInteger('network')->default(0);
            $table->string('qr_code')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->decimal('limit', 40, 20)->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
