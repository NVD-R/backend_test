<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('symbol');
            $table->string('icon');
            $table->boolean('is_fiat')->default(false);
            $table->string('contract')->nullable();
            $table->decimal('usd_rate', 40, 20)->default(0);
            $table->decimal('limit', 40, 20)->default(0);
            // $table->unsignedBigInteger('wallet_id')->nullable();
            // $table->string('wallet_address')->nullable();
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
