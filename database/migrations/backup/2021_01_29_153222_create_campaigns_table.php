<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('cover');
            $table->text('description');
            $table->unsignedBigInteger('goal');
            $table->unsignedBigInteger('remaining_goal');
            $table->boolean('is_success');
            $table->unsignedBigInteger('charity_id');
            $table->unsignedBigInteger('owner_id');
            $table->unsignedTinyInteger('status')->default(0);
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
