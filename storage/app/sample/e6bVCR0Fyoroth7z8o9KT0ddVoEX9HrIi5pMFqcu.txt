Order ID;Product;Category;Amount;Date;Country
1;Carrots;Vegetables;$4،270;2016/01/06;United States
2;Broccoli;Vegetables;$8،239;2016/01/07;United Kingdom
3;Banana;Fruit;$617;2016/01/08;United States
10;Apple;Fruit;$7،431;2016/01/16;Canada
5;Beans;Vegetables;$2،626;2016/01/10;Germany
6;Orange;Fruit;$3،610;2016/01/11;United States
7;Broccoli;Vegetables;$9،062;2016/01/11;Australia
8;Banana;Fruit;$6،906;2016/01/16;New Zealand
9;Apple;Fruit;$2،417;2016/01/16;France
54;Apple;Fruit;$4،364;2016/04/01;Canada
11;Banana;Fruit;$8،250;2016/01/16;Germany
12;Broccoli;Vegetables;$7،012;2016/01/18;United States
13;Carrots;Vegetables;$1،903;2016/01/20;Germany
64;Apple;Fruit;$2،763;2016/04/25;Canada
15;Apple;Fruit;$6،946;2016/01/24;France
16;Banana;Fruit;$2،320;2016/01/27;United Kingdom
17;Banana;Fruit;$2،116;2016/01/28;United States
18;Banana;Fruit;$1،135;2016/01/30;United Kingdom
19;Broccoli;Vegetables;$3،595;2016/01/30;United Kingdom
20;Apple;Fruit;$1،161;2016/02/02;United States
21;Orange;Fruit;$2،256;2016/02/04;France
22;Banana;Fruit;$1،004;2016/02/11;New Zealand
108;Apple;Fruit;$521;2016/06/04;Canada
24;Banana;Fruit;$4،582;2016/02/17;United States
25;Beans;Vegetables;$3،559;2016/02/17;United Kingdom
26;Carrots;Vegetables;$5،154;2016/02/17;Australia
27;Mango;Fruit;$7،388;2016/02/18;France
28;Beans;Vegetables;$7،163;2016/02/18;United States
29;Beans;Vegetables;$5،101;2016/02/20;Germany
30;Apple;Fruit;$7،602;2016/02/21;France
31;Mango;Fruit;$1،641;2016/02/22;United States
32;Apple;Fruit;$8،892;2016/02/23;Australia
33;Apple;Fruit;$2،060;2016/02/29;France
34;Broccoli;Vegetables;$1،557;2016/02/29;Germany
35;Apple;Fruit;$6،509;2016/03/01;France
36;Apple;Fruit;$5،718;2016/03/04;Australia
37;Apple;Fruit;$7،655;2016/03/05;United States
38;Carrots;Vegetables;$9،116;2016/03/05;United Kingdom
39;Banana;Fruit;$2،795;2016/03/15;United States
40;Banana;Fruit;$5،084;2016/03/15;United States
41;Carrots;Vegetables;$8،941;2016/03/15;United Kingdom
42;Broccoli;Vegetables;$5،341;2016/03/16;France
144;Apple;Fruit;$7،333;2016/08/27;Canada
44;Banana;Fruit;$9،400;2016/03/19;Australia
45;Beans;Vegetables;$6،045;2016/03/21;Germany
46;Apple;Fruit;$5،820;2016/03/22;New Zealand
47;Orange;Fruit;$8،887;2016/03/23;Germany
48;Orange;Fruit;$6،982;2016/03/24;United States
49;Banana;Fruit;$4،029;2016/03/26;Australia
50;Carrots;Vegetables;$3،665;2016/03/26;Germany
51;Banana;Fruit;$4،781;2016/03/29;France
52;Mango;Fruit;$3،663;2016/03/30;Australia
53;Apple;Fruit;$6،331;2016/04/01;France
205;Apple;Fruit;$2،455;2016/12/20;Canada
55;Carrots;Vegetables;$607;2016/04/03;United Kingdom
56;Banana;Fruit;$1،054;2016/04/06;New Zealand
57;Carrots;Vegetables;$7،659;2016/04/06;United States
58;Broccoli;Vegetables;$277;2016/04/12;Germany
59;Banana;Fruit;$235;2016/04/17;United States
60;Orange;Fruit;$1،113;2016/04/18;Australia
61;Apple;Fruit;$1،128;2016/04/21;United States
4;Banana;Fruit;$8،384;2016/01/10;Canada
63;Banana;Fruit;$4،387;2016/04/23;United States
23;Banana;Fruit;$3،642;2016/02/14;Canada
65;Banana;Fruit;$7،898;2016/04/27;United Kingdom
66;Banana;Fruit;$2،427;2016/04/30;France
67;Banana;Fruit;$8،663;2016/05/01;New Zealand
68;Carrots;Vegetables;$2،789;2016/05/01;Germany
69;Banana;Fruit;$4،054;2016/05/02;United States
70;Mango;Fruit;$2،262;2016/05/02;United States
71;Mango;Fruit;$5،600;2016/05/02;United Kingdom
72;Banana;Fruit;$5،787;2016/05/03;United States
43;Banana;Fruit;$135;2016/03/19;Canada
74;Banana;Fruit;$474;2016/05/05;Germany
75;Apple;Fruit;$4،325;2016/05/05;France
76;Banana;Fruit;$592;2016/05/06;United States
77;Orange;Fruit;$4،330;2016/05/08;United States
78;Banana;Fruit;$9،405;2016/05/08;United Kingdom
79;Apple;Fruit;$7،671;2016/05/08;France
80;Carrots;Vegetables;$5،791;2016/05/08;United Kingdom
81;Banana;Fruit;$6،007;2016/05/12;Canada
82;Banana;Fruit;$5،030;2016/05/14;Germany
83;Carrots;Vegetables;$6،763;2016/05/14;United Kingdom
84;Banana;Fruit;$4،248;2016/05/15;Australia
85;Banana;Fruit;$9،543;2016/05/16;France
86;Broccoli;Vegetables;$2،054;2016/05/16;United Kingdom
87;Beans;Vegetables;$7،094;2016/05/16;Germany
88;Carrots;Vegetables;$6،087;2016/05/18;United States
89;Apple;Fruit;$4،264;2016/05/19;Australia
90;Mango;Fruit;$9،333;2016/05/20;United States
91;Mango;Fruit;$8،775;2016/05/22;Germany
92;Broccoli;Vegetables;$2،011;2016/05/23;United Kingdom
93;Banana;Fruit;$5،632;2016/05/25;United States
94;Banana;Fruit;$4،904;2016/05/25;New Zealand
95;Beans;Vegetables;$1،002;2016/05/25;Australia
96;Orange;Fruit;$8،141;2016/05/26;United Kingdom
111;Banana;Fruit;$6،941;2016/06/20;Canada
98;Orange;Fruit;$1،380;2016/05/26;Australia
99;Broccoli;Vegetables;$8،354;2016/05/26;Germany
100;Banana;Fruit;$5،182;2016/05/27;United States
101;Apple;Fruit;$2،193;2016/05/27;France
102;Mango;Fruit;$3،647;2016/05/28;United States
103;Apple;Fruit;$4،104;2016/05/28;United States
104;Carrots;Vegetables;$7،457;2016/05/28;United States
119;Banana;Fruit;$8،530;2016/07/05;Canada
106;Broccoli;Vegetables;$4،685;2016/05/30;Germany
107;Banana;Fruit;$3،917;2016/06/04;United States
196;Banana;Fruit;$136;2016/12/12;Canada
109;Apple;Fruit;$5،605;2016/06/10;France
110;Broccoli;Vegetables;$9،630;2016/06/11;Germany
14;Broccoli;Vegetables;$2،824;2016/01/22;Canada
112;Broccoli;Vegetables;$7،231;2016/06/20;United Kingdom
113;Broccoli;Vegetables;$8،891;2016/06/23;Australia
114;Banana;Fruit;$107;2016/06/25;France
115;Banana;Fruit;$4،243;2016/06/26;United States
116;Orange;Fruit;$4،514;2016/06/27;United States
117;Mango;Fruit;$5،480;2016/07/02;United States
118;Banana;Fruit;$5،002;2016/07/02;France
62;Broccoli;Vegetables;$9،231;2016/04/22;Canada
120;Orange;Fruit;$4،819;2016/07/07;New Zealand
121;Broccoli;Vegetables;$6،343;2016/07/11;United Kingdom
122;Orange;Fruit;$2،318;2016/07/13;United Kingdom
123;Orange;Fruit;$220;2016/07/20;United Kingdom
124;Orange;Fruit;$6،341;2016/07/20;New Zealand
125;Apple;Fruit;$330;2016/07/20;Germany
126;Broccoli;Vegetables;$3،027;2016/07/20;United Kingdom
127;Orange;Fruit;$850;2016/07/22;New Zealand
128;Banana;Fruit;$8،986;2016/07/23;United Kingdom
129;Broccoli;Vegetables;$3،800;2016/07/25;United States
130;Carrots;Vegetables;$5،751;2016/07/28;United Kingdom
131;Apple;Fruit;$1،704;2016/07/29;United Kingdom
132;Banana;Fruit;$7،966;2016/07/30;Australia
133;Banana;Fruit;$852;2016/07/31;United States
134;Beans;Vegetables;$8،416;2016/07/31;Australia
135;Banana;Fruit;$7،144;2016/08/01;France
136;Broccoli;Vegetables;$7،854;2016/08/01;United States
137;Orange;Fruit;$859;2016/08/03;United States
138;Broccoli;Vegetables;$8،049;2016/08/12;United States
139;Banana;Fruit;$2،836;2016/08/13;Germany
140;Carrots;Vegetables;$1،743;2016/08/19;United States
141;Apple;Fruit;$3،844;2016/08/23;France
142;Apple;Fruit;$7،490;2016/08/24;France
143;Broccoli;Vegetables;$4،483;2016/08/25;Germany
155;Broccoli;Vegetables;$352;2016/09/09;Canada
145;Carrots;Vegetables;$7،654;2016/08/28;United States
146;Apple;Fruit;$3،944;2016/08/29;United Kingdom
147;Beans;Vegetables;$5،761;2016/08/29;Germany
148;Banana;Fruit;$6،864;2016/09/01;New Zealand
149;Banana;Fruit;$4،016;2016/09/01;Germany
150;Banana;Fruit;$1،841;2016/09/02;United States
151;Banana;Fruit;$424;2016/09/05;Australia
152;Banana;Fruit;$8،765;2016/09/07;United Kingdom
153;Banana;Fruit;$5،583;2016/09/08;United States
154;Broccoli;Vegetables;$4،390;2016/09/09;New Zealand
105;Mango;Fruit;$3،767;2016/05/29;Canada
156;Apple;Fruit;$8،489;2016/09/11;United States
157;Banana;Fruit;$7،090;2016/09/11;France
158;Banana;Fruit;$7،880;2016/09/15;United States
159;Orange;Fruit;$3،861;2016/09/18;United States
160;Broccoli;Vegetables;$7،927;2016/09/19;Germany
161;Banana;Fruit;$6،162;2016/09/20;United States
162;Mango;Fruit;$5،523;2016/09/25;Australia
163;Broccoli;Vegetables;$5،936;2016/09/25;United Kingdom
164;Carrots;Vegetables;$7،251;2016/09/26;Germany
165;Orange;Fruit;$6،187;2016/09/27;Australia
166;Banana;Fruit;$3،210;2016/09/29;Germany
167;Carrots;Vegetables;$682;2016/09/29;Germany
168;Banana;Fruit;$793;2016/10/03;Australia
169;Carrots;Vegetables;$5،346;2016/10/04;Germany
170;Banana;Fruit;$7،103;2016/10/07;New Zealand
171;Carrots;Vegetables;$4،603;2016/10/10;United States
172;Apple;Fruit;$8،160;2016/10/16;France
173;Apple;Fruit;$7،171;2016/10/23;United Kingdom
174;Banana;Fruit;$3،552;2016/10/23;New Zealand
175;Banana;Fruit;$7،273;2016/10/25;Australia
176;Banana;Fruit;$2،402;2016/10/26;Germany
177;Banana;Fruit;$1،197;2016/10/26;Australia
178;Beans;Vegetables;$5،015;2016/10/26;Australia
179;Orange;Fruit;$5،818;2016/11/02;United States
180;Banana;Fruit;$4،399;2016/11/03;United Kingdom
181;Carrots;Vegetables;$3،011;2016/11/03;United States
182;Apple;Fruit;$4،715;2016/11/09;United Kingdom
183;Apple;Fruit;$5،321;2016/11/12;France
184;Banana;Fruit;$8،894;2016/11/15;United States
185;Carrots;Vegetables;$4،846;2016/11/25;United Kingdom
186;Broccoli;Vegetables;$284;2016/11/25;Germany
187;Orange;Fruit;$8،283;2016/11/26;United Kingdom
73;Orange;Fruit;$6،295;2016/05/03;Canada
189;Banana;Fruit;$9،014;2016/11/28;Australia
190;Apple;Fruit;$1،942;2016/11/29;France
191;Banana;Fruit;$7،223;2016/11/30;United States
192;Carrots;Vegetables;$4،673;2016/12/02;United States
193;Carrots;Vegetables;$9،104;2016/12/04;France
194;Apple;Fruit;$6،078;2016/12/05;United States
195;Beans;Vegetables;$3،278;2016/12/06;Germany
97;Orange;Fruit;$3،644;2016/05/26;Canada
197;Banana;Fruit;$8،377;2016/12/12;Australia
198;Banana;Fruit;$2،382;2016/12/12;United States
199;Banana;Fruit;$8،702;2016/12/15;Germany
200;Banana;Fruit;$5،021;2016/12/16;United States
201;Apple;Fruit;$1،760;2016/12/16;Australia
202;Banana;Fruit;$4،766;2016/12/18;Germany
203;Beans;Vegetables;$1،541;2016/12/19;United Kingdom
204;Orange;Fruit;$2،782;2016/12/20;United Kingdom
188;Orange;Fruit;$9،990;2016/11/28;Canada
206;Apple;Fruit;$4،512;2016/12/22;New Zealand
207;Apple;Fruit;$8،752;2016/12/22;Germany
208;Carrots;Vegetables;$9،127;2016/12/25;United States
209;Apple;Fruit;$1،777;2016/12/28;France
210;Beans;Vegetables;$680;2016/12/28;France
211;Orange;Fruit;$958;2016/12/29;United States
212;Carrots;Vegetables;$2،613;2016/12/29;Australia
213;Carrots;Vegetables;$339;2016/12/30;Australia
320;Apple;Fruit;$1،234;2016/02/23;Australia
3200;Apple;Fruit;$5،678;2016/02/23;Australia

