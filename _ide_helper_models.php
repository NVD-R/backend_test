<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Ability
 *
 * @property int $id
 * @property string $name
 * @property int $ring_id
 * @property int $elixir
 * @property string|null $description
 * @property string $icon
 * @property int $limit
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $color
 * @method static \Illuminate\Database\Eloquent\Builder|Ability newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ability newQuery()
 * @method static \Illuminate\Database\Query\Builder|Ability onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability query()
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereElixir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereRingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Ability whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Ability withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Ability withoutTrashed()
 */
	class Ability extends \Eloquent {}
}

namespace App\Models\Admin{
/**
 * App\Models\Admin\Admin
 *
 * @property int $id
 * @property string $fullname
 * @property string $mobile
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $is_super_admin
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|Admin search($term)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Admin whereUpdatedAt($value)
 */
	class Admin extends \Eloquent {}
}

namespace App\Models\Admin{
/**
 * App\Models\Admin\Token
 *
 * @property int $id
 * @property string $access_token
 * @property int $user_id
 * @property int $client_id
 * @property string $ip
 * @property string $agent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Admin\Admin $user
 * @method static \Illuminate\Database\Eloquent\Builder|Token newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Token newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Token query()
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUserId($value)
 */
	class Token extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Answer
 *
 * @property int $id
 * @property string $text
 * @property int $question_id
 * @property bool $is_correct
 * @property string|null $description
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|Answer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Answer newQuery()
 * @method static \Illuminate\Database\Query\Builder|Answer onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Answer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Answer withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Answer withoutTrashed()
 */
	class Answer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property string|null $description
 * @property int $type
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Query\Builder|Category onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category search($term)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Category withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Category withoutTrashed()
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Model
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Model newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Model query()
 */
	class Model extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notif
 *
 * @property int $id
 * @property int $owner_id
 * @property object $notif
 * @property $type
 * @property $fcm_status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|Notif newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Notif newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif query()
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereFcmStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereNotif($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Notif whereUpdatedAt($value)
 */
	class Notif extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OneToOneAbility
 *
 * @property int $id
 * @property int $user_id
 * @property int $race_id
 * @property int $question_id
 * @property int $ability_id
 * @property int|null $ability_position
 * @property int $ability_elixir
 * @property int $ability_time
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility query()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereAbilityElixir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereAbilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereAbilityPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereAbilityTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAbility whereUserId($value)
 */
	class OneToOneAbility extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OneToOneAnswer
 *
 * @property int $id
 * @property int $user_id
 * @property int $race_id
 * @property int $question_id
 * @property int $answer_id
 * @property int $answer_position
 * @property int $answer_time
 * @property int $is_correct
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer query()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereAnswerPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereAnswerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneAnswer whereUserId($value)
 */
	class OneToOneAnswer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OneToOneQuestion
 *
 * @property int $id
 * @property int $race_id
 * @property int $question_id
 * @property int $answer_a_id
 * @property int $answer_b_id
 * @property int $answer_c_id
 * @property int $answer_d_id
 * @property int $correct_answer_id
 * @property int $answer_time
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Answer $answerA
 * @property-read \App\Models\Answer $answerB
 * @property-read \App\Models\Answer $answerC
 * @property-read \App\Models\Answer $answerD
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereAnswerAId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereAnswerBId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereAnswerCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereAnswerDId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereAnswerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereCorrectAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneQuestion whereUpdatedAt($value)
 */
	class OneToOneQuestion extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OneToOneRace
 *
 * @property int $id
 * @property int $ring_id
 * @property int|null $winner_id
 * @property int|null $user1_id
 * @property int $user1_is_bot
 * @property int|null $user1_category1_id
 * @property int|null $user1_category2_id
 * @property int $user1_status
 * @property int|null $user2_id
 * @property int $user2_is_bot
 * @property int|null $user2_category1_id
 * @property int|null $user2_category2_id
 * @property int $user2_status
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace query()
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereRingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser1Category1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser1Category2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser1IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser1Status($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser2Category1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser2Category2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser2IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereUser2Status($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OneToOneRace whereWinnerId($value)
 */
	class OneToOneRace extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Question
 *
 * @property int $id
 * @property string $text
 * @property int $category_id
 * @property string|null $description
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property object|null $answers
 * @property-read \App\Models\Answer|null $answer
 * @property-read \App\Models\Category $category
 * @method static \Illuminate\Database\Eloquent\Builder|Question newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Question newQuery()
 * @method static \Illuminate\Database\Query\Builder|Question onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Question query()
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereAnswers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Question whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Question withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Question withoutTrashed()
 */
	class Question extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RaceOneToOne
 *
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|RaceOneToOne newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceOneToOne newQuery()
 * @method static \Illuminate\Database\Query\Builder|RaceOneToOne onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceOneToOne query()
 * @method static \Illuminate\Database\Query\Builder|RaceOneToOne withTrashed()
 * @method static \Illuminate\Database\Query\Builder|RaceOneToOne withoutTrashed()
 */
	class RaceOneToOne extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Ring
 *
 * @property-read \App\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|Ring newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Ring newQuery()
 * @method static \Illuminate\Database\Query\Builder|Ring onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Ring query()
 * @method static \Illuminate\Database\Query\Builder|Ring withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Ring withoutTrashed()
 */
	class Ring extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SoloGame
 *
 * @property int $id
 * @property string $uuid
 * @property int $ring_id
 * @property object|null $players
 * @property object|null $categories
 * @property object|null $questions
 * @property object|null $answers
 * @property object|null $abilities
 * @property $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame query()
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereAbilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereAnswers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame wherePlayers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereRingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SoloGame whereUuid($value)
 */
	class SoloGame extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamAbility
 *
 * @property int $id
 * @property int $user_id
 * @property int $race_id
 * @property int $question_id
 * @property int $ability_id
 * @property int $ability_elixir
 * @property int $ability_time
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereAbilityElixir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereAbilityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereAbilityTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAbility whereUserId($value)
 */
	class TeamAbility extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamAnswer
 *
 * @property int $id
 * @property int $user_id
 * @property int $race_id
 * @property int $question_id
 * @property int $answer_id
 * @property int $answer_position
 * @property int $answer_time
 * @property int $is_correct
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereAnswerPosition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereAnswerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereIsCorrect($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamAnswer whereUserId($value)
 */
	class TeamAnswer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamQuestion
 *
 * @property int $id
 * @property int $race_id
 * @property int $question_id
 * @property int $answer_a_id
 * @property int $answer_b_id
 * @property int $answer_c_id
 * @property int $answer_d_id
 * @property int $correct_answer_id
 * @property int $answer_time
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Answer $answerA
 * @property-read \App\Models\Answer $answerB
 * @property-read \App\Models\Answer $answerC
 * @property-read \App\Models\Answer $answerD
 * @property-read \App\Models\Question $question
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereAnswerAId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereAnswerBId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereAnswerCId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereAnswerDId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereAnswerTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereCorrectAnswerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereQuestionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamQuestion whereUpdatedAt($value)
 */
	class TeamQuestion extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamRace
 *
 * @property int $id
 * @property int $ring_id
 * @property int|null $hero_id
 * @property int|null $user1_id
 * @property int $user1_is_bot
 * @property int $user1_status
 * @property int|null $user2_id
 * @property int $user2_is_bot
 * @property int $user2_status
 * @property int|null $user3_id
 * @property int $user3_is_bot
 * @property int $user3_status
 * @property int|null $user4_id
 * @property int $user4_is_bot
 * @property int $user4_status
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereHeroId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereRingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser1Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser1IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser1Status($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser2Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser2IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser2Status($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser3Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser3IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser3Status($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser4Id($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser4IsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamRace whereUser4Status($value)
 */
	class TeamRace extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Token
 *
 * @property int $id
 * @property string $access_token
 * @property int $user_id
 * @property int $client_id
 * @property string $ip
 * @property string $agent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Token newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Token newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|Token query()
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereAccessToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereAgent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Token whereUserId($value)
 */
	class Token extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TournamentGame
 *
 * @property int $id
 * @property object|null $players
 * @property mixed|null $categories
 * @property object|null $questions
 * @property object|null $answers
 * @property object|null $abilities
 * @property string $start_time
 * @property $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property mixed|null $winners
 * @property int|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame query()
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereAbilities($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereAnswers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame wherePlayers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereQuestions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TournamentGame whereWinners($value)
 */
	class TournamentGame extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $fullname
 * @property string $mobile
 * @property string $mobile_verify_at
 * @property int $rank
 * @property int $ring_id
 * @property int $elixir
 * @property int $gem
 * @property string|null $fcm_token
 * @property int $is_bot
 * @property int $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $avatar
 * @property object|null $result
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Model orderBys($orders)
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User search($term)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereElixir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFcmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFullname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereGem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsBot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereMobileVerifyAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereResult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

